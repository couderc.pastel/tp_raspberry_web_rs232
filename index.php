<!doctype html>
<html>
	<head>
	  <title>Authentification</title>
	  <meta charset="utf-8" />
	  <link rel="stylesheet" href="css/style-sql.css" />
	</head>

	<body>
		<header>
			<!-- Menu de navigation du site dans l'en tête -->
			<h1>Raspberry PI : serveur WEB</h1>
			<nav>
				 
			</nav>
		</header>
		
		<section>
		<!-- Contenu principal -->
			<h2>Authentifiez vous !</h2>
			
			<div id="gauche">
				<img src="img/framboise.jpg" alt="framboise" class="image" width=200px/>
			</div>
			
			<div id="droite">
				<p>
					Saisissez le mot de passe. <!-- Prompt du mot de passe -->
				</p>
				<div id="authen">
					<br />
					<form method="post" action="authentification.php"> 
						<input type="password" name="pass"/>
						<input type="submit" value="Valider"/>
					</form>
				</div>
			</div>

		</section>
		<?php
			require_once('footer.php');
		?>

	
	</body>
</html>