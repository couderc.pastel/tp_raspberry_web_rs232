<!doctype html>
<html>
	<head>
	  <title>Authentification</title>
	  <meta charset="utf-8" />
	  <link rel="stylesheet" href="css/style-sql.css" />
	</head>

	<body>
		<header>
			<!-- Menu de navigation du site dans l'en tête -->
			<h1>Raspberry PI : serveur WEB</h1>
			<nav>
				 
			</nav>
		</header>
		
		<section>
		<!-- Contenu principal -->
			<h2>Authentifiez vous !</h2>
			
			<div id="gauche">
				<img src="img/framboise.jpg" alt="framboise" class="image" width=200px/>
			</div>
			
			<div id="droite">
				<p>Mot de passe erroné (Pour le moment c'est "azerty")</p> <!-- L'utilisateur n'est pas authentifié : On affiche le message d'erreur -->
				<div id="authen">
					<br/>
					<form method="post" action="authentification.php"> 
						<input type="password" name="pass"/>
						<input type="submit" value="Valider"/>
					</form>
				</div>
			</div>
		</section>
		
		<footer>
			<!-- Signer et dater la page, c'est une question de politesse! -->
			<p>Conçu et réalisé en 2014<br/>
			par P. Couderc - Lycée Déodat de Séverac - Toulouse</p>
		</footer>
	
	</body>
</html>